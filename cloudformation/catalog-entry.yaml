---
AWSTemplateFormatVersion: 2010-09-09
Description: Service Catalog product for GitLab runner
Parameters:
  PortfolioId:
    Description: The portfolio ID of an existing portfolio, if applicable. Leave blank to create one.
    Type: String
Conditions:
  cExistingPortfolio: !Not [ !Equals [ !Ref PortfolioId, "" ] ]
Resources:
  Product:
    Type: AWS::ServiceCatalog::CloudFormationProduct
    Properties:
      Owner: JHC Technology
      # Distributor:
      # SupportDescription:
      # SupportEmail:
      Description: GitLab Runner
      AcceptLanguage: en
      SupportUrl: https://gitlab.com/jhctech-oss/gitlab-dind-runner
      Tags:
        - Key: Application
          Value: GitLab-Runner
      Name: GitLab-Runner
      ProvisioningArtifactParameters: # each item is a version
        -
          Name: v1.1.3
          Description: Fix regression bug with multi-token support
          Info:
            LoadTemplateFromURL: "https://jhc-quickstarts.s3.amazonaws.com/gitlab-runner/product.template"

  Portfolio:
    Type: AWS::ServiceCatalog::Portfolio
    Properties:
      ProviderName: JHC Technology
      DisplayName: JHC Technology Portfolio

  ProductAssociation:
    Type: AWS::ServiceCatalog::PortfolioProductAssociation
    Properties:
      # SourcePortfolioId: String
      PortfolioId: !If
        - cExistingPortfolio
        - !Ref PortfolioId
        - !Ref Portfolio
      ProductId: !Ref Product
      AcceptLanguage: en

  LaunchConstraintRole:
    Type: AWS::IAM::Role
    Metadata:
      cfn_nag:
        rules_to_suppress:
          - id: F3 ## revisit later
            reason: Allowing all actions within relevant services for simplicity.
          - id: F38 ## revisit later
            reason: We do not know ahead of time the full list of roles the product will need to access since many are dynamically created.
          - id: W11 ## revisit later
            reason: We do not know ahead of time the full list of resources the product will need to access since many are dynamically created.
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service: servicecatalog.amazonaws.com
            Action: sts:AssumeRole
      Policies:
        - PolicyName: CFN-Permissions
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - cloudformation:CreateStack
                  - cloudformation:DeleteStack
                  - cloudformation:DescribeStackEvents
                  - cloudformation:DescribeStacks
                  - cloudformation:GetTemplateSummary
                  - cloudformation:SetStackPolicy
                  - cloudformation:ValidateTemplate
                  - cloudformation:UpdateStack
                  - s3:GetObject
                Resource: "*"
              - Effect: Allow
                Action: ssm:GetParameters
                Resource: !Sub arn:${AWS::Partition}:ssm:${AWS::Region}:*:parameter/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2
        - PolicyName: GitLabRunner-Launch-Permissions
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              -
                Effect: Allow
                Action:
                  - autoscaling:*
                  - cloudformation:*
                  - ec2:*
                  - iam:*
                  - lambda:*
                  - sns:*
                  - s3:*
                Resource: "*"

  LaunchConstraint:
    Type: AWS::ServiceCatalog::LaunchRoleConstraint
    Properties:
      Description: Launch constraint role for GitLab Runner product
      AcceptLanguage: en
      PortfolioId: !If
        - cExistingPortfolio
        - !Ref PortfolioId
        - !Ref Portfolio
      ProductId: !Ref Product
      RoleArn: !GetAtt LaunchConstraintRole.Arn
    DependsOn: ProductAssociation
