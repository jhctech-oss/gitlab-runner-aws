## Created by Mike Atkinson

## The following CloudFormation deploys IAM resources
## which support GitLab Runner.
---
AWSTemplateFormatVersion: 2010-09-09
Description: GitLab runner IAM roles, policies, and instance profiles
Parameters:
  RunnerCacheBucket:
    Description: Name of S3 bucket for runner cache
    Type: String
  UseCase:
    Description: The work you would like to perform with this runner
    Type: String
    Default: CloudFormation
    AllowedValues:
      - Default
      - CloudFormation
      - Other IaC
      - S3
      - Custom

Conditions:
  CloudFormationUseCase: !Equals [ !Ref UseCase, CloudFormation ]
  CloudFormationOrS3UseCase: !Or [ !Equals [ !Ref UseCase, CloudFormation ], !Equals [ !Ref UseCase, S3 ] ]
  OtherIacUseCase: !Equals [ !Ref UseCase, Other IaC ]
  NotUsingCustomInstanceProfile: !Not [ !Equals [ !Ref UseCase, Custom ] ]

Resources:
  ControllerRole:
    Type: AWS::IAM::Role
    Metadata:
      cfn_nag:
        rules_to_suppress:
          - id: W11 ## revisit later
            reason: We do not know ahead of time the full list of resources the controller will need to access since many are dynamically created.
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service: !Sub ec2.${AWS::URLSuffix}
            Action: sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore
        - arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy
        - arn:aws:iam::aws:policy/ReadOnlyAccess
      Policies:
        - PolicyDocument:
            Version: 2012-10-17
            Statement:
              -
                Sid: AutoScalingLifecycle
                Effect: Allow
                Action:
                  - autoscaling:CompleteLifecycleAction
                  - autoscaling:RecordLifecycleActionHeartbeat
                Resource: '*'
          PolicyName: BootupPermissions

  ControllerPolicy:
    Type: AWS::IAM::Policy
    Metadata:
      cfn_nag:
        rules_to_suppress:
          - id: F4 ## revisit later
            reason: Allowing all S3 actions for simplicity.
          - id: F39 ## revisit later
            reason: We do not know what roles a customer may need to pass, so allow all.
          - id: W12 ## revisit later
            reason: We do not know ahead of time the full list of resources the controller will need to access since many are dynamically created.
    Properties:
      PolicyName: GitLabRunnerPolicy
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          -
            Sid: CloudformationGetPassRole
            Effect: Allow
            Action:
              - iam:GetRole
              - iam:PassRole
            Resource: '*'
          -
            Sid: CloudFormationManagement
            Effect: Allow
            Action:
              - cloudformation:*
              - s3:PutObject
            Resource: '*'
          -
            Sid: Ec2
            Effect: Allow
            Action:
              - ec2:*KeyPair*
              - ec2:*SecurityGroup*
              - ec2:CreateTags
              - ec2:RunInstances
              - ec2:StopInstances
              - ec2:StartInstances
              - ec2:TerminateInstances
            Resource: '*'
          -
            Sid: RunnerCacheBucketFullAccess
            Effect: Allow
            Action: s3:*
            Resource:
              - !Sub arn:aws:s3:::${RunnerCacheBucket}
              - !Sub arn:aws:s3:::${RunnerCacheBucket}/*
      Roles:
        - !Ref ControllerRole

  MachineRole:
    Condition: NotUsingCustomInstanceProfile
    Type: AWS::IAM::Role
    Metadata:
      cfn_nag:
        rules_to_suppress:
          - id: F3 ## revisit later
            reason: Allowing all S3 actions for simplicity.
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          -
            Effect: Allow
            Principal:
              Service: ec2.amazonaws.com
            Action: sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore
        - arn:aws:iam::aws:policy/ReadOnlyAccess
        - !If
          - OtherIacUseCase
          - arn:aws:iam::aws:policy/AdministratorAccess
          - !Ref "AWS::NoValue"
      Policies:
        - PolicyDocument:
            Version: 2012-10-17
            Statement:
              -
                Sid: RunnerCacheBucketFullAccess
                Effect: Allow
                Action: s3:*
                Resource:
                  - !Sub arn:aws:s3:::${RunnerCacheBucket}
                  - !Sub arn:aws:s3:::${RunnerCacheBucket}/*
          PolicyName: RunnerCacheBucketFullAccess

  rGitLabRunnerPolicyCloudFormation:
    Condition: CloudFormationUseCase
    Type: AWS::IAM::Policy
    Metadata:
      cfn_nag:
        rules_to_suppress:
          - id: F4 ## revisit later
            reason: Allowing all S3 actions for simplicity.
          - id: F39 ## revisit later
            reason: We do not know what roles a customer may need to pass, so allow all.
          - id: W12 ## revisit later
            reason: We do not know ahead of time the full list of resources the controller will need to access since many are dynamically created.
    Properties:
      PolicyName: GitLabRunnerCloudFormationPolicy
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          -
            Sid: CloudformationGetPassRole
            Effect: Allow
            Action:
              - iam:GetRole
              - iam:PassRole
            Resource: "*"
          -
            Sid: CloudFormationManagement
            Effect: Allow
            Action:
              - cloudformation:*
              - s3:PutObject
              - s3:GetObject # for change sets
            Resource: "*"
      Roles:
        - !Ref MachineRole

  rGitLabRunnerPolicyS3:
    Condition: CloudFormationOrS3UseCase
    Type: AWS::IAM::Policy
    Metadata:
      cfn_nag:
        rules_to_suppress:
          - id: W12 ## revisit later
            reason: We do not know ahead of time the full list of resources the controller will need to access since many are dynamically created.
    Properties:
      PolicyName: GitLabRunnerS3Policy
      PolicyDocument:
        Version: 2012-10-17
        Statement:
          -
            Sid: S3Management
            Effect: Allow
            Action:
              - s3:CreateBucket
              - s3:DeleteBucket
              - s3:DeleteObject
              - s3:GetBucketTagging
              - s3:GetBucketVersioning
              - s3:GetEncryptionConfiguration
              - s3:PutBucketTagging
              - s3:PutBucketVersioning
              - s3:PutEncryptionConfiguration
              - s3:PutObject
            Resource: "*"
      Roles:
        - !Ref MachineRole

  ControllerInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Roles:
        - !Ref ControllerRole

  MachineInstanceProfile:
    Condition: NotUsingCustomInstanceProfile
    Type: AWS::IAM::InstanceProfile
    Properties:
      Roles:
        - !Ref MachineRole

Outputs:
  ControllerInstanceProfile:
    Description: GitLab runner instance profile
    Value: !Ref ControllerInstanceProfile
  MachineInstanceProfile:
    Condition: NotUsingCustomInstanceProfile
    Description: GitLab runner instance profile
    Value: !Ref MachineInstanceProfile
