# AWS GitLab Runner

This is a GitLab Runner implementation on AWS. The runner uses the latest Amazon Linux 2 AMI provided by the public parameter `/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2`.

## GitLab Runner Service Catalog Product

Deploys a product registered to AWS Service Catalog available to users for launching their own GitLab runner EC2 instances in their own AWS accounts without writing any code.

### Common Use-Cases

This product is intended for users who have an AWS account, are using GitLab to manage project code, and would like to use GitLab CI to automate build, test, and deployment jobs of that code.

### Prerequisites

To use this product, you will need:

1. An AWS account
2. Appropriate permissions to launch or manage products in AWS Service Catalog
3. A GitLab project
4. A runner token provided by GitLab from the target project or group with which the runner will be registered

### Parameters

The following summarizes the parameters given in the Service Catalog product for AWS GitLab Runner. An in-depth explanation of the parameters can be found in the parameters heading.

#### Runner Registration Parameters

##### GitLabUrl

The URL of the GitLab server where you are registering the runner

##### RunnerName

A name to help identify your runner. All runners will also be suffixed with the EC2 instance Id.

##### RunnerTags

Tags help control which runners are allowed to run which jobs. All runners will be allowed to run untagged jobs, but tagged jobs will need to be run by a runner with matching tags. This is useful for separating `dev` jobs from `prod` jobs.

See GitLab's documentation to learn more about [runner tags](https://docs.gitlab.com/ee/ci/runners/#using-tags).

##### RunnerTokens

Runner tokens associate a runner with one or more GitLab projects or groups. The token ensures that no one else can use your runners without your permission.

See GitLab's documentation to learn about [specific runners and tokens](https://docs.gitlab.com/ee/ci/runners/#registering-a-specific-runner-with-a-project-registration-token).

#### Runner Permissions Parameters

##### `UseCase`

The `UseCase` parameter is designed to provide least-privilege AWS permissions to the GitLab runner instances for the use case of the runner. The following values can be entered:

| `UseCase` Value | Description |
| --------------- | ----------- |
| Default         | Designed for static code testing and interacting with non-AWS resources (e.g. creating Docker images); can be used for validating CloudFormation templates and other static code activities, but is designed not to have permissions for managing live AWS resources. |
| CloudFormation  | Designed for deployment of resources exclusively using AWS CloudFormation. Performs only the CloudFormation API actions and expects that you pass a role to the CloudFormation service to perform actions on resources. |
| Other IaC       | Designed for deployment of resources using a custom Infrastructure as Code tool. This option grants the runner instance the ability to perform all customer-approved actions in all available AWS services. Use with caution. |
| S3              | Designed for management of S3 objects and buckets only, e.g. deploying web artifacts to a bucket used for website hosting. |
| Custom          | Bring-your-own-role: designed for custom use cases where you have already designed an IAM role with a trust policy for EC2 and an instance profile. Specify this use case and the `CustomInstanceProfile` parameter as the name of the instance profile to attach that to your instances on launch. See the section below for minimum permissions required. |

All non-custom use cases attach the `arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore` and `arn:aws:iam::aws:policy/ReadOnlyAccess` policies. These permissions provide read access to all AWS resources and services and the ability for the instance to communicate with the Systems Manager service, including use of Systems Manager Session Manager for SSH terminal access.

###### Minimum Permissions for the Custom UseCase

If you bring your own instance profile, some minimum permissions are required in order to successfully launch GitLab runners. If you use a built-in use case, these permissions are supplied for you and are not prerequisites to provisioning this product.

###### Required Permissions for the Custom UseCase

The following permissions allow EC2 runners to inform the EC2 Auto Scaling service that they have completed successfully. EC2 Auto Scaling will terminate instances after a timeout period if the instances do not complete this step.

```yml
Effect: Allow
Action:
  - autoscaling:CompleteLifecycleAction
  - cloudformation:DescribeStackResource
Resource: "*"
```

###### Recommended Permissions for the Custom UseCase

The following permissions allows EC2 runners to send log and metric data to the CloudWatch service which can be helpful for monitoring. Runner instances do not require them to be created successfully.

```yml
Sid: CloudWatchMonitoring
Effect: Allow
Action:
  - cloudwatch:PutMetricData
  - logs:CreateLogGroup
  - logs:CreateLogStream
  - logs:PutLogEvents
  - logs:DescribeLogStreams
Resource: "*"
```

Alternatively, you can use the AWS managed policy `arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy` which contains the above permissions.

It is also recommended to attach the `arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore` policy for management through Systems Manager.

#### Machine scaling details

##### PeakMinMachines

The minimum number of machines available for CI jobs during peak hours. Each machine can serve one job at a time.

Another way to think of this value is "the number of jobs which should run with no delay during peak hours".

These machines are allowed to run during peak hours regardless of the `PeakIdleTime`.

##### ConcurrentMachines

The number of jobs which should be allowed to run concurrently at any one time during peak hours.

##### PeakMaxJobs

The total number of machines, active and idle, allowed to run at one time during peak hours.

##### PeakIdleTime

The amount of time idle instances may wait for additional jobs before shutting down during peak hours.

The number of machines specified by `PeakMinMachines` will be retained.

##### OffPeakIdleCount

The minimum number of machines available for CI jobs during off hours. Each machine can serve one job at a time.

Another way to think of this value is "the number of jobs which should run with no delay during off hours". Typically during off hours a delay is acceptable, so this value is typically `0`.

These machines are allowed to run during off hours regardless of the `OffPeakIdleTime`.

##### OffPeakIdleTime

The amount of time idle instances may wait for additional jobs before shutting down during off hours.

The number of machines specified by `OffPeakIdleCount` will be retained.

##### OffPeakPeriodWeekdays

A cron expression describing weekday off hours.

##### OffPeakPeriodWeekends

A cron expression describing weekend off hours.

##### OffPeakTimezone

The IANA timezone for the off peak periods.

#### Controller Scaling Details

##### PeakMinControllers

The minimum number of controller instances running during peak hours. Typical value is `1`, but may be higher depending on availability requirements.

##### DesiredCapacity

The desired number of controller instances running during peak hours. Typical value is `1`, but may be higher depending on availability requirements.

##### PeakMaxControllers

The maximum number of controller instances running during peak hours. Typical value is `1`, but may be higher depending on availability requirements.

##### InstanceType

The instance type for controller and machine instances.

##### VolumeSize

The volume size for the controller instance.

## Frequently Asked Questions

1. I already launched a GitLab Runner from the Service Catalog but need to update settings like the instance size or runner token or upgrade to a new version. What can I do?

    AWS Service Catalog provides a way to update a provisioned product. Refer to [AWS documentation](https://docs.aws.amazon.com/servicecatalog/latest/userguide/enduser-update.html) for updating provisioned products.

    Update actions are generally quicker than new create actions because AWS will only make modifications to resources which have changed. Because the GitLab runner EC2 instances run in an auto scaling group, old runners will only be removed from AWS once the new runners are online.

2. I updated values for my GitLab Runner provisioned product. The new runner and or updated values are not showing under my project's runner configuration why?

    When updating the runner product, you have to _manually_ remove the old runner from GitLab before updating the product at this time. We are exploring an automated way to remove runners once they are terminated.

3. I terminated the provisioned product, but some EC2 instances were left behind. Why did this happen?

    On termination, the product triggers a Lambda function called the "cleanup function". This function terminates any EC2 "docker-machine" instances. Since these instances are not directly managed by CloudFormation, CloudFormation cannot delete them.

    The function looks up these instances by querying for a tag key "Parent" with value equal to the instance Id of the machine controller. If this tag is modified, the function will fail to clean up these instances, leaving them orphaned.

4. I terminated the provisioned product, but I still see the runner in the GitLab UI. Why is this?

    We have not yet implemented an automated mechanism to remove instances from GitLab. Please follow #3 for additional details.

## Reporting Bugs or Issues

If you have questions, identified bugs, or would like additional features to be added to catalog-entry, please submit an `Issue` on this project and the support team will address.
